package com.zuitt.wdc044.models;

// allow access for spring boot package, allow data to store permanently in database
//
import javax.persistence.*;

// mark this Java object as a representation of a database table via @Entity
@Entity
// designate table name via @Table anotation, name of the table inside the database
@Table(name="posts")
public class Post {

    // indicate that this property represents the primary key @ID anotation
    @Id

    // values for this property will be auto-incremented - has only one system in numbering objects
    @GeneratedValue
    private Long id;

    // class properties that represents table columns in a rational databases are annotated as @Column
    @Column
    private String title;

    @Column
    private String content;

    //default constructor - this is needed when retrieving posts
    public Post(){

    }

    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public Long getId() {
        return id;
    }//    public void setId(Long id) {
//        this.id = id;
//    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
